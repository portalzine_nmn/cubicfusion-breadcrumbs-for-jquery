$.fn.buildBreadcrumb = function(options) {

	this.settings = {
    itemClass: 			'item',	
	startCapClass: 		'start',	
	beforeItemClass: 	'before', 
	afterItemClass: 	'after',   
	endCapClass: 		'end',
	textBreadcrumb:		false,
	textBreadcrumbSep:  "&nbsp;&#187;&nbsp;",
	wrapAllItems		: false
	
	
  	};
   

   if(options)
  	$.extend(this.settings, options);
   
  	var settings = this.settings;
 
	
	return this.each(function(a) {
		var size = $(this).find("li").size();
		
		$(this).find("li").filter(function(index){
			 					
    	
				$(this).addClass(settings.itemClass);
		    
				$(this).attr("id","item"+index);
							
				 if((index != size-1) || (settings.wrapAllItems == true )){
					if(settings.textBreadcrumb == true){
						$(this).after("<li id='after"+index+"' class='"+settings.afterItemClass+"'>"+settings.textBreadcrumbSep+"</li>");
					}else{
						if(settings.beforeItemClass != null)
						$(this).before("<li id='before"+index+"' class='"+settings.beforeItemClass+"'></li>");
						
						if(settings.afterItemClass != null)
						$(this).after("<li id='after"+index+"' class='"+settings.afterItemClass+"'></li>");
						
					}
				}
				
			
  		
		});
		
 		if(settings.startCapClass != null)
		$(this).find("li:first").before("<li class='"+settings.startCapClass+"'></li>");
	
		if(settings.endCapClass != null)
 		$(this).find("li:last").after("<li class='"+settings.endCapClass+"'></li>");
		
		
	
	});

}